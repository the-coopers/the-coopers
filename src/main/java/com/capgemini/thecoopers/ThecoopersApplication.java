package com.capgemini.thecoopers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThecoopersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThecoopersApplication.class, args);
	}

}
