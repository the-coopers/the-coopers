package com.capgemini.thecoopers.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.thecoopers.repository.model.Partida;
import com.capgemini.thecoopers.repository.model.ResultadoPartida;
import com.capgemini.thecoopers.service.impl.JuegoServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class JuegoController {
	
	private final JuegoServiceImpl juegoService;

	public JuegoController(JuegoServiceImpl juegoService) {
		this.juegoService = juegoService;
	}
	
	//Juego
	
	@PostMapping("/juego")
	/**
	 * Angular envía el nombre y elección para que generemos el juego y lo almacenemos en la BBDD
	 * @param nombreJugador
	 * @param eleccionJugador
	 */
	public ResultadoPartida agregar(@RequestBody Partida partida) {
		return juegoService.agregarPartida(partida.getNombreJugador(), partida.getEleccionJugador());
	}
	
	//Estadísticas
	@GetMapping("/estadisticas")
	public List<ResultadoPartida> estadisticas() {
		return juegoService.getEstadisticas();
	}
	

}
