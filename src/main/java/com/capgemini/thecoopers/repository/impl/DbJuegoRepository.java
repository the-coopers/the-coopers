package com.capgemini.thecoopers.repository.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.thecoopers.repository.model.Partida;

@Component
public class DbJuegoRepository {
	
	@Autowired
	private JpaFullRepository jpafr;

	public Collection<Partida> findAll() {
		return jpafr.findAll();
	}

	public Partida add(Partida partida) {
		return jpafr.save(partida);
	}

}
