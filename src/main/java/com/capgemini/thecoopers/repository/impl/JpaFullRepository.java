package com.capgemini.thecoopers.repository.impl;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capgemini.thecoopers.repository.model.Partida;

public interface JpaFullRepository extends JpaRepository<Partida, Integer> {

}
