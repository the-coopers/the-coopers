package com.capgemini.thecoopers.repository.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name= "PARTIDAS")
public class Partida {

	@Column
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPartida;
	
	@Column
	private String nombreJugador;
	
	@Column
	private int eleccionJugador;
	
	@Column
	private int eleccionOrdernador;
	
	@Column
	private Date fechaHora;
	
	@PrePersist
	  void fechaHora() {
	    this.fechaHora = new Date();
	  }
	
	
	
	public Partida(String pNombre, int pEleccionJugador, int pEleccionOrdenador) {
		this.nombreJugador = pNombre;
		this.eleccionJugador = pEleccionJugador;
		this.eleccionOrdernador = pEleccionOrdenador;
	}



	public Partida(String nombreJugador, int eleccionJugador) {
		super();
		this.nombreJugador = nombreJugador;
		this.eleccionJugador = eleccionJugador;
	}
}
