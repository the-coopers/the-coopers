package com.capgemini.thecoopers.repository.model;

import java.util.Date;

import com.capgemini.thecoopers.repository.model.logic.PiedraPapelTijeraFactory;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ResultadoPartida {
	private int idPartida;
	private String nombreJugador;
	private String eleccionJugador;
	private String eleccionOrdenador;
	private int resultado;
	private String descripcion;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date fecha;
	
	


public static ResultadoPartida convertirAResultado(Partida pPartida) {
	PiedraPapelTijeraFactory pptUsuario = PiedraPapelTijeraFactory.getInstance(pPartida.getEleccionJugador());
	PiedraPapelTijeraFactory pptOrdenador = PiedraPapelTijeraFactory.getInstance(pPartida.getEleccionOrdernador());
	
	int resultado = pptUsuario.comparar(pptOrdenador);
	String desc = pptUsuario.getDescripcionResultado();
	
	return new ResultadoPartida(pPartida.getIdPartida(), pPartida.getNombreJugador(),
			pptUsuario.getNombre(),
			pptOrdenador.getNombre(), 
			resultado,desc,
			pPartida.getFechaHora()
			);
}
}