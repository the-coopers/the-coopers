package com.capgemini.thecoopers.repository.model.logic;

public class Lagarto extends PiedraPapelTijeraFactory{

	public Lagarto() {
		this(LAGARTO, "lagarto");
	}
	
	public Lagarto(int pNumero, String pNombre) {
		super(pNumero, pNombre);
	}

	@Override
	public boolean isMe(int pNumero) {
		return pNumero == LAGARTO;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory) {
		int result = 0;
		int numeroRecibido =pPiedraPapelTijeraFactory.getNumero();
		
		switch(numeroRecibido) {
		case PAPEL:
		case SPOKE:
			result = 1; //GANA
			descripcionResultado = nombre + " gana con " + pPiedraPapelTijeraFactory.getNombre(); //nombre de la clase donde estoy pierde con el nombre de la clase
			break;
		
		case TIJERA:
		case PIEDRA:
			result = -1; //pierde
			descripcionResultado = nombre + " pierde con " + pPiedraPapelTijeraFactory.getNombre(); //nombre de la clase donde estoy pierde con el nombre de la clase
			break;
			
		default:
			result = 0; //empata
			descripcionResultado = nombre + " empata con " + pPiedraPapelTijeraFactory.getNombre(); //nombre de la clase donde estoy pierde con el nombre de la clase
			break;	
		
		}
		
		return result;
	}
	

}
