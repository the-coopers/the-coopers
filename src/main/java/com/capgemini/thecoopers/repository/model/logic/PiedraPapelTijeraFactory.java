package com.capgemini.thecoopers.repository.model.logic; //pais: es de españa; tipo proyecto:edu de educativo; empresa:alter o capgemini seria en mi caso; nombre proyecto: practica0 o piedraPapelTijera; luego iría modelo en este caso

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory { //yo no tenía abstract

	public static final int PIEDRA  = 1; //yo no tenía final, pero él quiere que sea constante y no se pueda modificar, al ser cte debo inicializarla con un valor, pongo 1 como podría poner cualquier otro valor

	public static final int PAPEL   = 2;
	
	public static final int TIJERA  = 3;
	
	public static final int LAGARTO = 4;
	
	public static final int SPOKE   = 5;
	
	//Atributos:
	
	protected String 								descripcionResultado;
	
	private static List<PiedraPapelTijeraFactory> 	elementos; //no tenía static (ojo que estaba subrayado)
	
	protected String 								nombre;
	
	protected int 									numero;	
	
	//Constructores:Yo había puesto el vacío, pero no es necesario aunque le meta el que lleva parámetros
	
	public PiedraPapelTijeraFactory(int pNumero, String pNombre) {
		super();
		this.numero = pNumero;
		this.nombre = pNombre;

	}
	
	//Getters, setters: accesos
	
	public String getNombre() {return nombre;}
	public void setNombre(String nombre) {this.nombre = nombre;}

	public int getNumero() {return numero;}
	public void setNumero(int numero) {this.numero = numero;}
	
	public String getDescripcionResultado() {return descripcionResultado;}
	
	//Métodos negocio:
	
	public abstract boolean isMe(int pNumero);

	public abstract int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory);

	public static PiedraPapelTijeraFactory getInstance(int pNumero) {
		
		//El padre conoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		elementos.add(new Lagarto());
		elementos.add(new Spoke());
		
		//recorro la lista de los hijos con forEach:
		for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if(piedraPapelTijeraFactory.isMe(pNumero))  //va preguntando si es él el que coincide, y cuando lo haga, lo retorna
				return piedraPapelTijeraFactory;
		}
		
		return null; //y si no encuentra ninguno que coincida retorna null
	}

}














