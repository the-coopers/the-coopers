package com.capgemini.thecoopers.repository.model.logic;

public class Tijera extends PiedraPapelTijeraFactory{

	public Tijera() {
		this(TIJERA, "tijera");
	}
	
	public Tijera(int pNumero,  String pNombre) {
		super(pNumero, pNombre);
		

	}	

	@Override
	public boolean isMe(int pNumero) {
		return pNumero == TIJERA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory) {
		int result = 0;
		int numeroRecibido =pPiedraPapelTijeraFactory.getNumero();
		
		switch(numeroRecibido) {
		case PIEDRA:
		case SPOKE:
			result = -1; //pierde
			descripcionResultado = nombre + " pierde con " + pPiedraPapelTijeraFactory.getNombre(); //nombre de la clase donde estoy pierde con el nombre de la clase
			break;
		
		case PAPEL:
		case LAGARTO:
			result = 1; //gana
			descripcionResultado = nombre + " gana con " + pPiedraPapelTijeraFactory.getNombre(); //nombre de la clase donde estoy pierde con el nombre de la clase
			break;
			
		default:
			result = 0; //pierde
			descripcionResultado = nombre + " empata con " + pPiedraPapelTijeraFactory.getNombre(); //nombre de la clase donde estoy pierde con el nombre de la clase
			break;	
		
		}
		
		return result;
	}
	
}
