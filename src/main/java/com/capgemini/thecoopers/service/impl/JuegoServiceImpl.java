package com.capgemini.thecoopers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.capgemini.thecoopers.repository.impl.JpaFullRepository;
import com.capgemini.thecoopers.repository.model.Partida;
import com.capgemini.thecoopers.repository.model.ResultadoPartida;
import com.capgemini.thecoopers.repository.model.logic.PiedraPapelTijeraFactory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class JuegoServiceImpl{
	
	private final JpaFullRepository juegoRepository;
	    
	public List<Partida> getPartidas() {
        return juegoRepository.findAll();
	}
	 
    public Partida agregarPartida(Partida partida) {
       juegoRepository.save(partida);
       return partida;
    }

	public ResultadoPartida agregarPartida(String pNombreJugador, int pEleccionJugador) {
		int EleccionOrdenador = (int) (Math.random()*5) + 1; 
		Partida partida = new Partida(pNombreJugador, pEleccionJugador, EleccionOrdenador);
		agregarPartida(partida);
		//retornaremos un resultado partida
		return ResultadoPartida.convertirAResultado(partida);
	}
	
	public List<ResultadoPartida> getEstadisticas(){
		List<Partida> partidasLista = juegoRepository.findAll();
		List<ResultadoPartida> resultadoPartidas = new ArrayList<ResultadoPartida>();
		for (Partida partida : partidasLista) {
			resultadoPartidas.add(ResultadoPartida.convertirAResultado(partida));
		}
		return resultadoPartidas;
	}
}













