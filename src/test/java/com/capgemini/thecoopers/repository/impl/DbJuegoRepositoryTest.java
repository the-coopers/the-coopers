package com.capgemini.thecoopers.repository.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.capgemini.thecoopers.repository.model.Partida;

@SpringBootTest
class DbJuegoRepositoryTest {
	
	
	Partida partidaTest;
	@Autowired
	DbJuegoRepository repository;
	JpaFullRepository jpafr;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		//aca agrego cuatro registros de testeo, utilizando jdbc
		//uno para eliminar
		//otro para modificar
		//otros dos para leer.
		Class.forName("com.mysql.cj.jdbc.Driver");
	      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/juegos","root","admin");
		     Statement sql = con.createStatement();
		     sql.execute("delete from partidas");
		     sql.execute("insert into partidas(eleccion_jugador, eleccion_ordernador,fecha_hora,nombre_jugador)"
		              + "values (2,3,'2022-12-22 15:52:27.427000','patricia')");
		     sql.execute("insert into partidas(eleccion_jugador, eleccion_ordernador,fecha_hora,nombre_jugador)"
		              + "values (2,3,'2022-12-22 15:52:27.427000','marta')");
		     sql.execute("insert into partidas(eleccion_jugador, eleccion_ordernador,fecha_hora,nombre_jugador)"
		              + "values (2,3,'2022-12-22 15:52:27.427000','marina')");
		     sql.execute("insert into partidas(eleccion_jugador, eleccion_ordernador,fecha_hora,nombre_jugador)"
		              + "values (2,3,'2022-12-22 15:52:27.427000','lucy')");
	      sql.close();
	      con.close();

	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
	      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/juegos","root","admin");
		     Statement sql = con.createStatement();
		     sql.execute("delete from partidas");
	      sql.close();
	      con.close();
	}

	@BeforeEach
	void setUp() throws Exception {
		partidaTest = new Partida("Gabriel_test", 1, 2);
	}

	@AfterEach
	void tearDown() throws Exception {
		partidaTest = null;
	}

	@Test
	void testFindAll() {
		System.out.println("todos= " + repository.findAll());
		assertEquals(5, repository.findAll().size());
		
	}

	@Test
	void testAdd() {
		repository.add(partidaTest);
		assertEquals( "Gabriel_test", partidaTest.getNombreJugador());
	}

}
