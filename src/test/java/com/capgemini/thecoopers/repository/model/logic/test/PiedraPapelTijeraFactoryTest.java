package com.capgemini.thecoopers.repository.model.logic.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.capgemini.thecoopers.repository.model.logic.Lagarto;
import com.capgemini.thecoopers.repository.model.logic.Papel;
import com.capgemini.thecoopers.repository.model.logic.Piedra;
import com.capgemini.thecoopers.repository.model.logic.PiedraPapelTijeraFactory;
import com.capgemini.thecoopers.repository.model.logic.Spoke;
import com.capgemini.thecoopers.repository.model.logic.Tijera;

class PiedraPapelTijeraFactoryTest {

	//creo mi lote de pruebas:
	
		PiedraPapelTijeraFactory 	piedra, 
									papel, 
									tijera,
									lagarto,
									spoke;
		

		@BeforeEach
		void setUp() throws Exception {
			piedra = new Piedra();
			papel = new Papel();
			tijera = new Tijera();
			lagarto = new Lagarto();
			spoke = new Spoke();
		}

		@AfterEach
		void tearDown() throws Exception {
			piedra = null;
			papel =  null;
			tijera = null;
			lagarto = null;
			spoke = null;
		}

		@Test
		void testGetInstancePiedra() {
			assertTrue(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA) instanceof Piedra);
		}
		
		@Test
		void testGetInstancePapel() {
			assertTrue(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL) instanceof Papel);
		}
		
		@Test
		void testGetInstanceTijera() {
			assertTrue(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA) instanceof Tijera);
		}
		
		@Test
		void testGetInstanceLagarto() {
			assertTrue(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LAGARTO) instanceof Lagarto);
		}
		
		@Test
		void testGetInstanceSpoke() {
			assertTrue(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SPOKE) instanceof Spoke);
		}

		@Test
		void testGetInstanceTijera_False() {
			assertFalse(PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL) instanceof Tijera);
		}
		
		@Test
		void testPiedraGanaATijera() {
			assertEquals(1, piedra.comparar(tijera));
		}
		
		@Test
		void testPiedraPierdeConPapel() {
			assertEquals(-1, piedra.comparar(papel));
		}
		
		@Test
		void testPiedraEmpataConPiedra() {
			Piedra piedra2 = new Piedra();
			assertEquals(0, piedra.comparar(piedra2));
		}
		
		@Test
		void testTijeraPierdeConPiedra() {
			assertEquals(-1, tijera.comparar(piedra));
		}
		
		@Test
		void testTijeraGanaAPapel() {
			assertEquals(1, tijera.comparar(papel));
		}
		
		
		@Test
		void testTijeraEmpataConTijera() {
			Tijera tijera2 = new Tijera()
	;		assertEquals(0, tijera.comparar(tijera2));
		}	
		
		@Test
		void testPapelEmpataConPapel() {
			//Papel papel2 = new Papel();
			assertEquals(0, papel.comparar(new Papel()));
		}
		
		@Test
		void testPapelPierdeConTijera() {
			assertEquals(-1, papel.comparar(tijera));
		}
		
		@Test 
		void testPapelEmpataConPapelResultados() {
			papel.comparar(new Papel());
			assertEquals("papel empata con papel", papel.getDescripcionResultado());
		}
		//habría que comparar el resto de textos también
		
		@Test
		void testLagartoGanaAPapel() {
			assertEquals(1, lagarto.comparar(papel));
		}
		
		@Test
		void testLagartoGanaASpoke() {
			assertEquals(1, lagarto.comparar(spoke));
		}
		
		@Test
		void testLagartoPierdeConPiedra() {
			assertEquals(-1, lagarto.comparar(piedra));
		}
		
		@Test
		void testLagartoPierdeConTijera() {
			assertEquals(-1, lagarto.comparar(tijera));
		}
		
		@Test
		void testLagartoEmpataConLagarto() {
			
			assertEquals(0, lagarto.comparar(new Lagarto()));
		}
		
		@Test
		void testSpokeGanaAPiedra() {
			assertEquals(1, lagarto.comparar(papel));
		}
		
		@Test
		void testSpokeGanaATijera() {
			assertEquals(1, spoke.comparar(tijera));
		}
		
		@Test
		void testSpokePierdeConLagarto() {
			assertEquals(-1, spoke.comparar(lagarto));
		}
		
		@Test
		void testSpokePierdeConPapel() {
			assertEquals(-1, spoke.comparar(papel));
		}
		
		@Test
		void testSpokeEmpataConSpoke() {
			
			assertEquals(0, spoke.comparar(new Spoke()));
		}

}
